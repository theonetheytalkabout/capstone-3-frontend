//Sanitize the data from the API. It will remove the comma and convert strings into numbers

export default function toNum(str) {
	//Ex: "25,222,223"
	//Convert string to array to gain access to array methods by using the spread operators
	const arr = [...str]
	// if we console this, console.log(arr) ["2", "5", "2", "2"...]
	const filteredArr = arr.filter(element => element !== ",")
	// filter out commas in the string
	return parseInt(filteredArr.reduce((x, y) => x + y))
	//reduces array back to single string

	/*reduce()
	on the first iteration:

	x is the first item in the array
	y is the 2nd item

	"2" + "5" = "25"
	"25" + "2" = "252"

	Cases of USA
	31,097,154
	3
	1
	31
	0
	310


	*/
}