import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// import Card from 'react-bootstrap/Card';
// import Button from 'react-bootstrap/Button';
import { Card, Button } from 'react-bootstrap';

export default function Course({course}) {

    // console.log(props);
    const { name, description, price, start_date, end_date } = course;

    // getters and setters
    const [count, setCount] = useState(0);
    // Use state hook for getting and setting the seats for this Course
    const [seats, setSeats] = useState(10);
    // State hook that indicates availability of course for enrollment
    const [isOpen, setIsOpen] = useState(true);

    function enroll() {
        setCount(count + 1);
        console.log('Enrollees: ' + count);
        setSeats(seats - 1);
        console.log('Seats: ' + seats);
    }

    /*
    const count = 0;
    count = 1;
    useState[0]
    useState[1]
    count[PHP-Laravel]
    count[Python-Django]
    count[Java-Springboot]
    */

    useEffect(() => {

		if(seats === 0){
            setIsOpen(false);
        }

    }, [seats])

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Card.Subtitle>Start Date:</Card.Subtitle>
                <Card.Text>{start_date}</Card.Text>
                <Card.Subtitle>End Date:</Card.Subtitle>
                <Card.Text>{end_date}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Card.Text>Seats: {seats}</Card.Text>
                { isOpen ? 
                        <Button variant="primary" onClick={enroll}>Enroll</Button>
                    : 
                        <Button variant="danger" disabled>Not Available</Button>
                }
            </Card.Body>
        </Card>
    )
}

Course.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}

/*
Course {
    props: {
        key: "wdc001",
        course: {
            name: "PHP-Laravel",
            description: "Lorem ipsum..."
            price: 45000
        }
    }
}
*/