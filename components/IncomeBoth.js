import React, { useContext, useState, useEffect } from 'react';
import AppHelper from '../app-helper';
import Head from 'next/head';
import { Doughnut, Pie } from 'react-chartjs-2';
import getRandomColor from 'randomcolor';
import { Form, Button, Row, Col } from 'react-bootstrap';
import moment from 'moment';
import MonthlyIncome from '../components/MonthlyIncome';
import MonthlyExpense from '../components/MonthlyExpense';
import LineChart from '../components/LineChart';
import IncomePie from '../components/IncomePie';
import ExpensePie from '../components/ExpensePie';
import View from '../components/View';
import {Card, CardText, CardBody, CardTitle, CardSubtitle, CardImg, CardLink} from 'reactstrap';
import ListGroup from 'react-bootstrap/ListGroup';

export default function IncomeBoth() {

	const [transactions, setTransactions] = useState([]);
	const [categories, setCategories] = useState([]);

	const [categoryTotal, setCategoryTotal] = useState([]);
	const [labels, setLabels] = useState([]);
	const [bgColors, setBgColors] = useState([]);
	const [search, setSearch] = useState('');
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	
	// console.log(startDate)
	// console.log(endDate)

	useEffect(() => {

    	fetch(`${ AppHelper.API_URL }/users/details`, {
        	method: 'GET',
        	headers: { 'Content-Type': 'application/json',
        	'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
	    	})
	    	.then(AppHelper.toJSON)
			.then(data => {
				console.log(data)
				setTransactions(data.transactions)
				setCategories(data.categories)
		})
	}, [])


	useEffect(() => {

		let totals = []
		let dates = []

		categories.forEach(category => {

			let totalPrice = 0
			transactions.map(data => {

				if (startDate !== "" && endDate !== "") {
					if (moment(data.transactedOn).isBetween(startDate, endDate, null, '[]') == true) {
						if (category.name == data.category) {
						totalPrice = totalPrice + data.price
						dates.push(data.transactedOn)
						}
					}
				} else {
					if (category.name == data.category) {
						totalPrice = totalPrice + data.price
						dates.push(data.transactedOn)
					}
				}
			})
			// console.log(totalPrice)
			totals.push(totalPrice)
		})
		// console.log(totals)
		setCategoryTotal(totals)

	}, [categories, startDate, endDate])



	useEffect(() => {
		let names = []
		categories.forEach(category => {
			// console.log(category.name)
			names.push(category.name)
			// console.log(names)
		})

		setLabels(names)


	}, [categories])

	console.log(labels)

	useEffect(() => {

		setBgColors(labels.map(() => getRandomColor()))

	}, [labels])
	// console.log(categoryTotal)

	const doughnutChartSettings = {
		datasets: [{
			data: categoryTotal,
			cutout: 0,
			backgroundColor: bgColors
		}],
		labels:  labels
	}

	return (
		<React.Fragment>
			<Form.Group controlId="startDate" id="dateBar">
                    <Form.Control 
                        type="date"
                        onChange={(e) => setStartDate(e.target.value)}
                        required
                    />
            </Form.Group>
            <Form.Group controlId="endDate" id="dateBar">
                    <Form.Control 
                        type="date" 
                        onChange={(e) => setEndDate(e.target.value)}
                        required
                    />
            </Form.Group>
			<Pie data ={doughnutChartSettings} redraw={false}/>
		</React.Fragment>
	)
}
