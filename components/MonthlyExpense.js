import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { Form, Button, Alert, Row, Col } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';
import AppHelper from '../app-helper';

export default function MonthlyExpense() {

	const [months, setMonths] = useState([]);
	const [monthlyExpense, setMonthlyExpense] = useState([]);
	const [transactions, setTransactions] = useState([]);

	useEffect(() => {

    	fetch(`${ AppHelper.API_URL }/users/details`, {
        	method: 'GET',
        	headers: { 'Content-Type': 'application/json',
        	'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
	    	})
	    	.then(AppHelper.toJSON)
			.then(data => {

				let expenseMonthly = []

				data.transactions.forEach(datum => {
					if (datum.type == "Expense") {
						expenseMonthly.push(datum)
					}
				})

				console.log(expenseMonthly)

				setTransactions(expenseMonthly)
				// setCategories(data.categories)
		})
	}, [])
	
	useEffect(() => {

		let tempMonths = [];

		transactions.map(record => {
			console.log(record.transactedOn)

			if(!tempMonths.find(month => month === moment(record.transactedOn).format('MMMM'))) {
				tempMonths.push(moment(record.transactedOn).format('MMMM'))
			}
		})

		// console.log(tempMonths)

		const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

		tempMonths.sort((a,b) => {

				// Checks if month exists in both arrays
				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1) {

					return monthsRef.indexOf(a) - monthsRef.indexOf(b);

				}

			})

			// console.log(tempMonths);
			setMonths(tempMonths);

	}, [transactions])

	useEffect(() => {

		setMonthlyExpense(months.map(month => {

			let prices = 0;

			transactions.forEach(record => {

				if(month === moment(record.transactedOn).format('MMMM')) {
					prices = prices + parseInt(record.price);
				}

			})

			return prices;

		}))

	}, [months])

	console.log(monthlyExpense)


	const data = {
		labels: months,
		datasets: [
			{
				label: 'Monthly Expense in 2021',
				backgroundColor: 'rgba(0,99,132,0.2)',
				borderColor: 'rgba(0,99,132,1)',
				borderWidth: 1,
				hoverBackgroundColor: 'rgba(0,99,132,0.4)',
				hoverBorderColor: 'rgba(0,99,132,1)',
				data: monthlyExpense
			}
		]
	}

	return(
		
		<Bar data={data} />
	)
}