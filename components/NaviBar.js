import React, { useContext, useEffect, useState } from 'react';
import Link from 'next/link';
// import React, { Component } from 'react';
import Navbar from  'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

export default function NaviBar() {

	const { user } = useContext(UserContext);

	// console.log(user)

	return (
		<Navbar bg="dark" variant="dark" expand="lg">
		  <Link href="/">
		  	<a className="navbar-brand">Budget Control</a>
		  </Link>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="ml-auto">
		      
		      
		      {(user.id !== null) ? 
		      	<React.Fragment>
		      		<Link href="/home">
						<a className="nav-link" role="button" id="nav">Home</a>
					</Link>
					<Link href="/categoryBreakdown">
					  	<a className="nav-link" role="button" id="nav">Breakdown</a>
					</Link>
			      	<Link href="/logout">
						<a className="nav-link" role="button" id="nav">Logout</a>
					</Link>
				</React.Fragment>
		      	:
		      	<React.Fragment>
		      		<Link href="/login">
						<a className="nav-link" role="button" id="nav">Login</a>
					</Link>
		      		<Link href="/register">
						<a className="nav-link" role="button" id="nav">Register</a>
					</Link>
		      	</React.Fragment>
		      }		      
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}

// const express = require('express');

/*export default class NaviBar extends Component {
	render() {
		return (
			<Navbar bg="light" expand="lg">
			  <Navbar.Brand href="#home">Zuitter</Navbar.Brand>
			  <Navbar.Toggle aria-controls="basic-navbar-nav" />
			  <Navbar.Collapse id="basic-navbar-nav">
			    <Nav className="ml-auto">
			      <Nav.Link href="#home">Home</Nav.Link>
			      <Nav.Link href="#courses">Courses</Nav.Link>
			    </Nav>
			  </Navbar.Collapse>
			</Navbar>
			)
	}
}*/