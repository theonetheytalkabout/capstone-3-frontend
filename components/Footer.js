import React, { useContext, useEffect, useState } from 'react';
import Link from 'next/link';
// import React, { Component } from 'react';
import Navbar from  'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

export default function NaviBar() {

	const { user } = useContext(UserContext);

	return (
		<Navbar bg="dark" variant="dark" expand="lg" className="text-white">
		  <p className="w-100 text-center d-flex justify-content-center">Budget Control | Patrick Servigon © 2021</p>
		</Navbar>
	)
}