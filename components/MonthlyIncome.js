import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { Form, Button, Alert, Row, Col } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';
import AppHelper from '../app-helper';

export default function MonthlyIncome() {

	const [months, setMonths] = useState([]);
	const [monthlyIncome, setMonthlyIncome] = useState([]);
	const [transactions, setTransactions] = useState([]);

	useEffect(() => {

    	fetch(`${ AppHelper.API_URL }/users/details`, {
        	method: 'GET',
        	headers: { 'Content-Type': 'application/json',
        	'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
	    	})
	    	.then(AppHelper.toJSON)
			.then(data => {

				let incomeMonthly = []

				data.transactions.forEach(datum => {
					if (datum.type == "Income") {
						incomeMonthly.push(datum)
					}
				})

				console.log(incomeMonthly)

				setTransactions(incomeMonthly)
				// setCategories(data.categories)
		})
	}, [])
	
	useEffect(() => {

		let tempMonths = [];

		transactions.map(record => {
			if(!tempMonths.find(month => month === moment(record.transactedOn).format('MMMM'))) {
				tempMonths.push(moment(record.transactedOn).format('MMMM'))
			}
		})

		console.log(tempMonths)

		const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

		tempMonths.sort((a,b) => {

				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1) {

					return monthsRef.indexOf(a) - monthsRef.indexOf(b);

				}

			})

			console.log(tempMonths);
			setMonths(tempMonths);

	}, [transactions])

	useEffect(() => {

		setMonthlyIncome(months.map(month => {

			let prices = 0;

			transactions.forEach(record => {

				if(month === moment(record.transactedOn).format('MMMM')) {
					prices = prices + parseInt(record.price);
				}

			})

			return prices;

		}))

	}, [months])

	console.log(monthlyIncome)


	const data = {
		labels: months,
		datasets: [
			{
				label: 'Monthly Income in 2021',
				backgroundColor: 'rgba(255,99,132,0.2)',
				borderColor: 'rgba(255,99,132,1)',
				borderWidth: 1,
				hoverBackgroundColor: 'rgba(255,99,132,0.4)',
				hoverBorderColor: 'rgba(255,99,132,1)',
				data: monthlyIncome
			}
		]
	}

	return(
		
		<Bar data={data} />
	)
}