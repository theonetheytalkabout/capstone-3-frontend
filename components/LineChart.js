import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { Form, Button, Alert, Row, Col } from 'react-bootstrap';
import { Bar, Line } from 'react-chartjs-2';
import moment from 'moment';
import AppHelper from '../app-helper';
import toNum from '../helpers/toNum'

export default function LineChart() {

	const [months, setMonths] = useState([]);
	const [monthlyIncome, setMonthlyIncome] = useState([]);
	const [labels, setLabels] = useState([]);
	const [balances, setBalances] = useState([]);

	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');

	console.log(startDate)
	console.log(endDate)

	useEffect(() => {

    	fetch(`${ AppHelper.API_URL }/users/details`, {
        	method: 'GET',
        	headers: { 'Content-Type': 'application/json',
        	'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
	    	})
	    	.then(AppHelper.toJSON)
			.then(data => {

				let balanceNumbers = []
				let labelNames = []

				data.transactions.forEach(record => {
					//console.log(record.transactedOn)
					// console.log(moment(record.transactedOn).isBetween(startDate, endDate, null, '[]'))
					if (startDate !== "" && endDate !== "") {
						if (moment(record.transactedOn).isBetween(startDate, endDate, null, '[]') == true) {
							balanceNumbers.push(record.currentBalance)
							labelNames.push(moment(record.transactedOn).format('DD MMM YYYY'))
							
						}
					} else {
							balanceNumbers.push(record.currentBalance)
							labelNames.push(moment(record.transactedOn).format('DD MMM YYYY'))
					}


					
				})

				setLabels(labelNames)
				setBalances(balanceNumbers)
		})

	}, [startDate, endDate])

	console.log(balances)



	const data = {
		labels: labels,
		datasets: [
			{
				label: 'Budget Trend',
				backgroundColor: 'rgba(255,99,132,0.2)',
				borderColor: 'rgba(255,99,132,1)',
				borderWidth: 1,
				hoverBackgroundColor: 'rgba(255,99,132,0.4)',
				hoverBorderColor: 'rgba(255,99,132,1)',
				data: balances
			}
		]
	}

	return(
		<React.Fragment>
		<Form.Group controlId="startDate">
                <Form.Control 
                    type="date"
                    onChange={(e) => setStartDate(e.target.value)}
                    required
                />
        </Form.Group>
        <Form.Group controlId="endDate">
                <Form.Control 
                    type="date" 
                    onChange={(e) => setEndDate(e.target.value)}
                    required
                />
        </Form.Group>
		<Line data={data} />
		</React.Fragment>
	)
}