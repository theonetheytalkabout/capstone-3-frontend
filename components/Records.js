import React, { useContext, useState, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import { Form, Button, Row, Col, Modal } from 'react-bootstrap';
import AppHelper from '../app-helper';
import UserContext from '../UserContext';
import Link from 'next/link'
import ListGroup from 'react-bootstrap/ListGroup';
import Swal from 'sweetalert2';
import {Card, CardText, CardBody, CardTitle, CardSubtitle, CardImg, CardLink, CardFooter} from 'reactstrap';
import RecordData from './RecordData';

export default function Records() {
	return (
        <RecordCards/>
		)
}

const RecordCards = () => {

	const [filter, setFilter] = useState('');
	const [search, setSearch] = useState('');
	const [transactions, setTransactions] = useState([]);
	const [balance, setBalance] = useState(0);
	const [term, setTerm] = useState('');
	const [modalIsOpen2, setModalIsOpen2] = useState(true);
	// console.log(modalIsOpen2)

	// ADD RECORDS STATES
	const [modalIsOpen, setModalIsOpen] = useState(false);
	const [categoryModalIsOpen, setCategoryModalIsOpen] = useState(false);
	console.log(categoryModalIsOpen)
	const [isActive2, setIsActive2] = useState(false);
	const [description, setDescription] = useState('');
	const [type, setType] = useState('');
	const [price, setPrice] = useState('');
	const [category, setCategory] = useState('');
	const [expenseCategories, setExpenseCategories] = useState([]);
	const [incomeCategories, setIncomeCategories] = useState([]);
	const [transactionDate, setTransactionDate] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [categoryAdd, setCategoryAdd] = useState('');
	const [typeAdd, setTypeAdd] = useState('Income');

	function addRecord(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', 'Authorization' : `Bearer ${localStorage.getItem('token')}` },
			body: JSON.stringify({
				description: description,
				type: type,
				price: price,
				category: category,
				transactedOn: transactionDate
			})
		}

		fetch(`${ AppHelper.API_URL }/users/addRecord`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			setModalIsOpen(false)
			Swal.fire('Record added.', '', 'success')
		})

	}

	function addCategory(e) {

		e.preventDefault();

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', 'Authorization' : `Bearer ${localStorage.getItem('token')}` },
			body: JSON.stringify({
				category: categoryAdd,
				type: typeAdd
			})
		}

		fetch(`${ AppHelper.API_URL }/users/addCategory`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			// console.log(data)
			closeAddCategory()
			// setCategoryModalIsOpen(false)
		})

	}

    useEffect(() => {

    	fetch(`${ AppHelper.API_URL }/users/details`, {
        	method: 'GET',
        	headers: { 'Content-Type': 'application/json',
        	'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
	    	})
	    	.then(AppHelper.toJSON)
			.then(data => {
				//console.log(data)
				setBalance(data.balance)
				setTransactions(data.transactions.reverse())
				setModalIsOpen2(true)

				let eCategories = []
				let iCategories = []

				data.categories.map(category => {
					if (category.type == "Income") {
						iCategories.push(category)
					} else if (category.type == "Expense") {
						eCategories.push(category)
					}
				})

				setExpenseCategories(eCategories)
				setIncomeCategories(iCategories)
		})

	}, [modalIsOpen, modalIsOpen2, categoryModalIsOpen])

    useEffect(() => {

		if(description !== "" && (type == "Income" || type == "Expense" )){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [description, type])

    useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match
		if(categoryAdd !== "" && typeAdd !== ""){
			setIsActive2(true);
		} else {
			setIsActive2(false);
		}

	}, [categoryAdd, typeAdd])


	function openAddCategory() {
		setModalIsOpen(false)
		setCategoryModalIsOpen(true)
	}

	function closeAddCategory() {
		setModalIsOpen(true)
		setCategoryModalIsOpen(false)
	}

    const recordsData = transactions.map(data => {
    	// console.log(data.type)

    	if (data.type === filter) {
			if (term !== '') {

				if (data.description.toLowerCase().includes(term.toLowerCase())) {
					return <RecordData key={data._id} record={data} setModalIsOpen2={setModalIsOpen2}/>
				} else {
					return null
				}

			} else {

				return <RecordData key={data._id} record={data} />

			}
		} else if (filter !== "Income" && filter !== "Expense") {

			if (term !== '') {

				if ((data.description.toLowerCase().includes(term.toLowerCase())) || (data.status.toLowerCase().includes(term.toLowerCase())) || (data.category.toLowerCase().includes(term.toLowerCase()))) {
					return <RecordData key={data._id} record={data} setModalIsOpen2={setModalIsOpen2}/>
				} else {
					return null
				}

			} else {

				return <RecordData key={data._id} record={data} setModalIsOpen2={setModalIsOpen2}/>

			}

			
	  	}
    })

	return (
		<React.Fragment>
            <Card>
            <CardBody id="recordBody">
            	<h3 id="title" className="w-100 text-center d-flex justify-content-center">Records</h3>
            		<Row>
	            		<Col>
					  		<Button onClick={() => setModalIsOpen(true)} variant="dark" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">
								Add Record
							</Button>
					    </Col>
					    <Col>
						    <Form.Group controlId="search">
			                    <Form.Control 
			                        type="text" 
			                        placeholder="Search Records"
			                        onChange={(e) => setTerm(e.target.value)}
			                        required
			                    />
			          		</Form.Group>
						</Col>
					</Row>
						<Form.Group controlId="type">
							<div>
							<select className="w-100 text-center d-flex justify-content-center"
								value={filter}
								onChange={(e) => setFilter(e.target.value)}
								required
							>
								<option value="">All categories</option>
								<option value="Income">Income</option>
								<option value="Expense">Expense</option>
							</select>
							</div>	
						</Form.Group>

					  	{(transactions < 1) ?
					  		<h3 className="w-100 text-center d-flex justify-content-center">You have no records.</h3>
					  		:
					  		recordsData
					  	}

						
				</CardBody>
            </Card>


            <Modal show={modalIsOpen} onHide={() => setModalIsOpen(false)} ariaHideApp={false} id="modal">
            	<Modal.Header closeButton>
				    	<Modal.Title className="w-100 text-center d-flex justify-content-center">Add Record</Modal.Title>
			  	</Modal.Header>

				<Form onSubmit={(e) => addRecord(e)}>
				<Modal.Body>
					<Form.Group controlId="description">
						<Form.Label>Description</Form.Label>
						<Form.Control
							type="description"
							placeholder="Description"
							value={description}
							onChange={(e) => setDescription(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="type">
						<Form.Label>Category Type</Form.Label>
						<div>
						<select className="w-100 text-center d-flex justify-content-center"
							value={type}
							onChange={(e) => setType(e.target.value)}
							required
						>

							<option value="" disabled>Choose category type</option>
							<option value="Income">Income</option>
							<option value="Expense">Expense</option>
						</select>
						</div>	
					</Form.Group>

					<Form.Group controlId="name">
						<Form.Label>Category Name</Form.Label>
						<div>
						<select className="w-100 text-center d-flex justify-content-center" aria-label=".form-select-lg example"
							value={category}
							onChange={(e) => setCategory(e.target.value)}
							required
						>
							<option value="" disabled>Choose category name</option>

							{
								(type !== "")
								?
									(type == "Income")
									?
									incomeCategories.map(data => (
									<option key={data._id} value={data.name}>{data.name}</option>
								))
									:
									expenseCategories.map(data => (
									<option key={data._id} value={data.name}>{data.name}</option>
								))
								:
								null
							}

						</select>
						</div>
						<CardLink className="float-right" onClick={openAddCategory}>Add Category</CardLink>
					</Form.Group>

					<Form.Group controlId="price">
						<Form.Label>Price</Form.Label>
						<Form.Control
							type="number"
							placeholder="0"
							value={price}
							onChange={(e) => setPrice(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="transactionDate">
						<Form.Label>Date</Form.Label>
						<Form.Control
							type="date"
							onChange={(e) => setTransactionDate(e.target.value)}
							required
						/>
					</Form.Group>
				</Modal.Body>
				<Modal.Footer>
					{isActive ?
						<Button className="w-100 text-center d-flex justify-content-center" variant="primary" type="submit" id="submitBtn">
							Submit
						</Button>
						:
						<Button className="w-100 text-center d-flex justify-content-center" variant="danger" type="submit" id="submitBtn" disabled>
							Submit
						</Button>
					}
				</Modal.Footer>	

					
				</Form>
            </Modal>

            <Modal show={categoryModalIsOpen} onHide={() => setCategoryModalIsOpen(false)} id="addCategoryModal">
		    	<Modal.Header closeButton>
				    	<Modal.Title className="w-100 text-center d-flex justify-content-center">Add Category</Modal.Title>
			  	</Modal.Header>
		    	<Form onSubmit={(e) => addCategory(e)}>
		    		<Modal.Body>
						<Form.Group controlId="category">
							<Form.Label>Category Name</Form.Label>
							<Form.Control
								type="category"
								placeholder="Enter category name"
								value={categoryAdd}
								onChange={(e) => setCategoryAdd(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="type">
							<Form.Label>Category Type</Form.Label>
							<div>
							<select className="w-100 text-center d-flex justify-content-center"
								value={typeAdd}
								onChange={(e) => setTypeAdd(e.target.value)}
								required
							>
								<option value="Income">Income</option>
								<option value="Expense">Expense</option>
							</select>
							</div>	
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						{isActive2 ?
							<Button variant="primary" type="submit" id="submitBtn">
								Submit
							</Button>
							:
							<Button variant="danger" type="submit" id="submitBtn" disabled>
								Submit
							</Button>
						}
						<Button onClick={closeAddCategory} variant="outline-primary" id="submitBtn">
								Cancel
						</Button>
					</Modal.Footer>
				</Form>
		    </Modal>



		</React.Fragment>
	
	)

}