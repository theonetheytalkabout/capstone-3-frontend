import React, { useContext, useState, useEffect } from 'react';
import AppHelper from '../app-helper';
import Head from 'next/head';
import { Doughnut, Pie } from 'react-chartjs-2';
import getRandomColor from 'randomcolor';
import { Form, Button, Row, Col } from 'react-bootstrap';
import moment from 'moment';

export default function ExpensePie() {

	const [transactions, setTransactions] = useState([]);
	const [categories, setCategories] = useState([]);

	const [categoryTotal, setCategoryTotal] = useState([]);
	const [labels, setLabels] = useState([]);
	const [bgColors, setBgColors] = useState([]);
	const [search, setSearch] = useState('');
	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	
	// console.log(startDate)
	// console.log(endDate)

	useEffect(() => {

    	fetch(`${ AppHelper.API_URL }/users/details`, {
        	method: 'GET',
        	headers: { 'Content-Type': 'application/json',
        	'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
	    	})
	    	.then(AppHelper.toJSON)
			.then(data => {
				console.log(data.transactions)

				let transactionsExpense = []
				data.transactions.forEach(transaction => {
					if (transaction.type == "Expense") {
						transactionsExpense.push(transaction)
					}
				})

				console.log(transactionsExpense)

				let categoriesExpense = []
				data.categories.forEach(category => {
					if (category.type == "Expense") {
						categoriesExpense.push(category)
					}
				})

				setTransactions(transactionsExpense)
				setCategories(categoriesExpense)
		})
	}, [])


	useEffect(() => {

		console.log(transactions)

		let totals = []
		let dates = []

		categories.forEach(category => {

			let totalPrice = 0
			transactions.map(data => {

				if (startDate !== "" && endDate !== "") {
					if (moment(data.transactedOn).isBetween(startDate, endDate, null, '[]') == true) {
						if (category.name == data.category) {
						totalPrice = totalPrice + data.price
						dates.push(data.transactedOn)
						}
					}
				} else {
					if (category.name == data.category) {
						totalPrice = totalPrice + data.price
						dates.push(data.transactedOn)
					}
				}
			})
			// console.log(totalPrice)
			totals.push(totalPrice)
		})
		// console.log(totals)
		setCategoryTotal(totals)

	}, [categories, startDate, endDate])



	useEffect(() => {

		console.log(categories)
		let names = []
		categories.forEach(category => {
			// console.log(category.name)
			names.push(category.name)
			// console.log(names)
		})

		setLabels(names)


	}, [categories])

	console.log(labels)

	useEffect(() => {

		setBgColors(labels.map(() => getRandomColor()))

	}, [labels])
	// console.log(categoryTotal)

	const doughnutChartSettings = {
		datasets: [{
			data: categoryTotal,
			cutout: 0,
			backgroundColor: bgColors
		}],
		labels:  labels
	}

	return (
		<React.Fragment>
			<Form.Group controlId="startDate">
                    <Form.Control 
                        type="date"
                        onChange={(e) => setStartDate(e.target.value)}
                        required
                    />
            </Form.Group>
            <Form.Group controlId="endDate">
                    <Form.Control 
                        type="date" 
                        onChange={(e) => setEndDate(e.target.value)}
                        required
                    />
            </Form.Group>
			<Pie data ={doughnutChartSettings} redraw={false}/>
		</React.Fragment>
	)
}
