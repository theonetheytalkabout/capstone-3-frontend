import React, { select, useContext, useState, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import View from '../components/View';
import { Form, Button, Row, Col } from 'react-bootstrap';
import AppHelper from '../app-helper';
import UserContext from '../UserContext';

export default function EditProfile() {
    return (
        <View title={ ' Add Category ' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <ProfileForm/>
                </Col>
            </Row>
        </View>
        )
}

const ProfileForm = () => {
    
// State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [balance, setBalance] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Check if values are successfully binded
    // console.log(category);
    // console.log(type);

    const updateProfile = (e) => {

        e.preventDefault()

        fetch(`${ AppHelper.API_URL }/users/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                balance: balance
            })
        })
        .then(AppHelper.toJSON)
        .then(data => {
            Router.push('/profile')
        })

    }
    

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== "" && firstName !== "" && lastName !== "" && balance !== "")){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [email, firstName, lastName, balance])

    return (
        <React.Fragment>
            <Head>
                <title>Categories</title>
            </Head>
                    
        <Form onSubmit={(e) => updateProfile(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                    type="name"
                    placeholder="Enter first name"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                    type="name"
                    placeholder="Enter last name"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="balance">
                <Form.Label>Balance</Form.Label>
                <Form.Control
                    type="number"
                    placeholder="Enter balance"
                    value={balance}
                    onChange={(e) => setBalance(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                    type="number"
                    placeholder="Enter mobile number"
                    value={mobileNo}
                    onChange={(e) => setMobileNo(e.target.value)}
                    required
                />
            </Form.Group>

            {isActive ?
                <Button variant="primary" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">
                    Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled className="w-100 text-center d-flex justify-content-center">
                    Submit
                </Button>
            }

            
        </Form>
        </React.Fragment>
    )


}