import React, { useContext, useState, useEffect } from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import { Form, Button, Row, Col, Modal } from 'react-bootstrap';
import moment from 'moment';
// import Modal from 'react-modal';
import {Card, CardText, CardBody, CardTitle, CardSubtitle, CardImg, CardLink, CardFooter} from 'reactstrap';
import AppHelper from '../app-helper';

export default function RecordData({record, setModalIsOpen2}) {

	const [description, setDescription] = useState(record.description);
	const [type, setType] = useState(record.type);
	const [amount, setAmount] = useState(record.price);
	const [category, setCategory] = useState(record.category);
	const [status, setStatus] = useState(record.status)


	const [transactionDate, setTransactionDate] = useState(record.transactedOn);
	const [expenseCategories, setExpenseCategories] = useState([]);
	const [incomeCategories, setIncomeCategories] = useState([]);
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {

    	fetch(`${ AppHelper.API_URL }/users/details`, {
        	method: 'GET',
        	headers: { 'Content-Type': 'application/json',
        	'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
	    	})
	    	.then(AppHelper.toJSON)
			.then(data => {
				let eCategories = []
				let iCategories = []

				data.categories.map(category => {
					if (category.type == "Income") {
						iCategories.push(category)
					} else if (category.type == "Expense") {
						eCategories.push(category)
					}
				})

				setExpenseCategories(eCategories)
				setIncomeCategories(iCategories)
		})
	}, [])

	function updateRecord(e) {

		e.preventDefault();

		fetch(`${ AppHelper.API_URL }/users/updateRecord`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				recordId: record._id,
				description: description,
				type: type,
				status: status,
				price: amount,
				category: category
			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			//console.log(data)
			setModalIsOpen(false)
			setModalIsOpen2(false)
		})

	}

	function deleteRecord(e) {
		e.preventDefault()

		fetch(`${ AppHelper.API_URL }/users/`, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				recordId: record._id
			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			setModalIsOpen2(false)
		})


	}

	let price = record.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

	const [modalIsOpen, setModalIsOpen] = useState(false)

	return (
		<React.Fragment>

			<Card id="recordCard" className="card" key={record._id}>
			  	<CardBody className="">
			  		<Row>
			  			<Col xs={12} md={6}>
					    	<h5>{record.description}</h5>
				    	</Col>
				    	<Col id="priceCatCol" xs={12} md={6} className="text-md-right">
				    		{
							(record.type == "Income") ?
							<h5 className="text-success">+{price}</h5>
								:
							<h5 className="text-danger">–{price}</h5>
							}
				    	</Col>
				    </Row>
					<Row>
						<Col xs={12} md={6}>
							{
							(record.status == "Done") ?
							<h4 className="text-primary">{record.status}</h4>
								:
							<h4 className="text-danger">{record.status}</h4>
							}
						</Col>
						<Col id="priceCatCol" xs={12} md={6} className="text-md-right">
							<h5>{record.category}</h5>
						</Col>
					</Row>
						<h5>{moment(record.transactedOn).format('DD MMM YYYY')}</h5>
						
						
			  	</CardBody>

			  	<CardFooter id="cardfooter">
			  		<Row>
				  		<Col>
				  			<h7>₱{record.currentBalance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</h7>
			  			</Col>
			  			<Col className="text-md-right">
				  			<CardLink onClick={() => setModalIsOpen(true)}>Edit</CardLink>
							<CardLink onClick={(e) => deleteRecord(e)}>Delete</CardLink>
						</Col>
					</Row>
			  	</CardFooter>
			</Card>

			<Modal show={modalIsOpen} onHide={() => setModalIsOpen(false)} ariaHideApp={false}>

				<Modal.Header>
			    	<Modal.Title className="w-100 text-center d-flex justify-content-center">Update Record</Modal.Title>
			  	</Modal.Header>
	
		    	<Form onSubmit={(e) => updateRecord(e)}>
			    	<Modal.Body>
						<Form.Group controlId="description">
							<Form.Label>Description</Form.Label>
							<Form.Control
								type="description"
								placeholder="Description"
								value={description}
								onChange={(e) => setDescription(e.target.value)}
								required
							/>
						</Form.Group>
						<Row>
							<Col xs={12} md={6}>
								<Form.Group controlId="type">
									<Form.Label>Category Type</Form.Label>
									<div>
									<select className="w-100 text-center d-flex justify-content-center"
										value={type}
										onChange={(e) => setType(e.target.value)}
										required
									>

										<option value="" disabled>Choose category type</option>
										<option value="Income">Income</option>
										<option value="Expense">Expense</option>
									</select>
									</div>	
								</Form.Group>
							</Col>
							<Col xs={12} md={6}>
								<Form.Group controlId="name">
									<Form.Label>Category Name</Form.Label>
									<div>
									<select className="w-100 text-center d-flex justify-content-center" aria-label=".form-select-lg example"
										value={category}
										onChange={(e) => setCategory(e.target.value)}
										required
									>
										<option value="" disabled>Choose category name</option>

										{
											(type !== "")
											?
												(type == "Income")
												?
												incomeCategories.map(data => (
												<option key={data._id} value={data.name}>{data.name}</option>
											))
												:
												expenseCategories.map(data => (
												<option key={data._id} value={data.name}>{data.name}</option>
											))
											:
											null
										}

									</select>
									</div>	
								</Form.Group>
							</Col>
						</Row>
						<Row>
							<Col xs={12} md={6}>
								<Form.Group controlId="amount">
									<Form.Label>Price</Form.Label>
									<Form.Control
										type="number"
										placeholder="0"
										value={amount}
										onChange={(e) => setAmount(e.target.value)}
										required
									/>
								</Form.Group>
							</Col>
							<Col xs={12} md={6}>
								<Form.Group controlId="type">
									<Form.Label>Status</Form.Label>
									<div>
									<select className="w-100 text-center d-flex justify-content-center"
										value={status}
										onChange={(e) => setStatus(e.target.value)}
										required
									>
										<option value="Done">Done</option>
										<option value="Pending">Pending</option>
									</select>
									</div>	
								</Form.Group>
							</Col>
						</Row>
					</Modal.Body>

					<Modal.Footer>
						{
							isActive ?
							<Button variant="primary" type="submit" id="submitBtn" className="">
								Submit
							</Button>
							:
							<Button variant="danger" type="submit" id="submitBtn" disabled className="">
								Submit
							</Button>
						}
							<Button variant="outline-primary" onClick={() => setModalIsOpen(false)} id="submitBtn" className="">
								Cancel
							</Button>
					</Modal.Footer>
					
				</Form>
				    	
		    </Modal>
		</React.Fragment>
	)
}