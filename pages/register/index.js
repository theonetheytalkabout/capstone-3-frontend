import React, { useState, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../components/View';
import { Form, Button, Row, Col } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import {Card, CardText, CardBody, CardTitle, CardSubtitle, CardImg, CardLink, CardFooter} from 'reactstrap';

export default function index() {
	return (
		<View title={ ' Register ' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <RegisterForm/>
                </Col>
            </Row>
        </View>
		)
}

const RegisterForm = () => {

	
// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [balance, setBalance] = useState(0);
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	// console.log(email);
	// console.log(firstName)
	// console.log(lastName)
	// console.log(balance)
	// console.log(mobileNo)
	// console.log(password1);
	// console.log(password2);
	// console.log(isActive)

	function registerUser(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password1,
				firstName: firstName,
				lastName: lastName,
				balance: balance,
				mobileNo: mobileNo
			})
		}

		fetch(`${ AppHelper.API_URL }/users/`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			Router.push('/login')
		})

	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match
		if((email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "" && balance !== "") && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, password1, password2, firstName, lastName, balance])

	return (
		<React.Fragment>
            <Head>
                <title>Register</title>
            </Head>
        <Card>
        	<CardBody id="registerBody">
        		<h3 id="title" className="w-100 text-center d-flex justify-content-center">Register</h3>
				<Form onSubmit={(e) => registerUser(e)}>
					<Form.Group controlId="userEmail">
						<Form.Label>Email address</Form.Label>
						<Form.Control
							type="email"
							placeholder="Enter email"
							value={email}
							onChange={(e) => setEmail(e.target.value)}
							required
						/>
						<Form.Text className="text-muted">
							We'll never share your email with anyone else.
						</Form.Text>
					</Form.Group>

					<Form.Group controlId="firstName">
						<Form.Label>First Name</Form.Label>
						<Form.Control
							type="name"
							placeholder="Enter first name"
							value={firstName}
							onChange={(e) => setFirstName(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="lastName">
						<Form.Label>Last Name</Form.Label>
						<Form.Control
							type="name"
							placeholder="Enter last name"
							value={lastName}
							onChange={(e) => setLastName(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="balance">
						<Form.Label>Balance</Form.Label>
						<Form.Control
							type="number"
							placeholder="Enter balance"
							value={balance}
							onChange={(e) => setBalance(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="mobileNo">
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control
							type="number"
							placeholder="Enter mobile number"
							value={mobileNo}
							onChange={(e) => setMobileNo(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Password"
							value={password1}
							onChange={(e) => setPassword1(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="password2">
						<Form.Label>Verify Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Verify Password"
							value={password2}
							onChange={(e) => setPassword2(e.target.value)}
							required
						/>
					</Form.Group>

					{isActive ?
						<Button variant="primary" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">
							Submit
						</Button>
						:
						<Button variant="danger" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center" disabled>
							Submit
						</Button>
					}
					
				</Form>
			</CardBody>
		</Card>
		</React.Fragment>
	)


}