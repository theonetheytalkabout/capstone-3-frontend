import React, { useContext, useState, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../components/View';
import { Form, Button, Row, Col, Modal } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';
import Link from 'next/link'
import ListGroup from 'react-bootstrap/ListGroup';
import RecordData from '../../components/RecordData';
import Swal from 'sweetalert2';
import Records from '../../components/Records';

// import Modal from 'react-modal';
import {Card, CardText, CardBody, CardTitle, CardSubtitle, CardImg, CardLink, CardFooter } from 'reactstrap';

export default function index() {

	const [modalIsOpen, setModalIsOpen] = useState(false)
	const [modalIsOpen2, setModalIsOpen2] = useState(false);
	const [data, setData] = useState('')
	const [balanceString, setBalanceString] = useState('')
	const [socialsIsActive, setSocialsIsActive] = useState(false);

	useEffect(() => {

    	fetch(`${ AppHelper.API_URL }/users/details`, {
        	method: 'GET',
        	headers: { 'Content-Type': 'application/json',
        	'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
	    	})
	    	.then(AppHelper.toJSON)
			.then(data => {
				setData(data)
				console.log(data)
				console.log(data.instagram)
				setEmail(data.email)
				setFirstName(data.firstName)
				setLastName(data.lastName)
				setMobileNo(data.mobileNo)
				let stringBalance = data.balance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
				setBalanceString(stringBalance)
				setBalance(data.balance)
				setTwitter(data.twitter)
				setInstagram(data.instagram)
				setAddress(data.address)
				setFacebook(data.faceBook)
		})
	}, [modalIsOpen, modalIsOpen2, socialsIsActive])

	//Update Profile
	const [email, setEmail] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [balance, setBalance] = useState('');
    const [isActive, setIsActive] = useState(false);

    //Update Socials
    
    const [twitter, setTwitter] = useState('');
    const [instagram, setInstagram] = useState('');
    const [address, setAddress] = useState('');
    const [facebook, setFacebook] = useState('');

    //Update Password
    const [oldPassword, setOldPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    
    const [isActive2, setIsActive2] = useState(false);

    const updateProfile = (e) => {

        e.preventDefault()

        fetch(`${ AppHelper.API_URL }/users/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                balance: balance
            })
        })
        .then(AppHelper.toJSON)
        .then(data => {
        	Swal.fire('', 'Your profile has been updated.', 'success')
            setModalIsOpen(false)
        })

    }

    const addInstagram = (e) => {

    	e.preventDefault()

    	fetch(`${ AppHelper.API_URL }/users/updateSocials`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                instagram: instagram,
                faceBook: facebook,
                address: address,
                twitter: twitter
            })
        })
        .then(AppHelper.toJSON)
        .then(data => {
        	setSocialsIsActive(false)
        })

    }
    

    const updatePassword = (e) => {
    	e.preventDefault()

    	fetch(`${ AppHelper.API_URL }/users/updatePassword`, {
    		method: 'PUT',
    		headers: {
    			'Content-Type': 'application/json',
    			'Authorization': `Bearer ${localStorage.getItem('token')}`
    		},
    		body: JSON.stringify({
    			oldPassword: oldPassword,
    			newPassword: newPassword
    		})
    	})
    	.then(AppHelper.toJSON)
        .then(data => {
        	Swal.fire('', 'Your password has been updated.', 'success')
            setModalIsOpen2(false)
        })

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== "" && firstName !== "" && lastName !== "" && balance !== "")){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [email, firstName, lastName, balance])

    useEffect(() => {

        if((oldPassword !== "" && newPassword !== "" && verifyPassword !== "") && (verifyPassword == newPassword)){
            setIsActive2(true);
        } else {
            setIsActive2(false);
        }

    }, [oldPassword, newPassword, verifyPassword])


	return (
		<React.Fragment>
			<Head><title>Home</title></Head>
            <Row className="justify-content-center gutters-sm" id="profileRow">
                <Col md="4" mb="3">
                    <Card className="card" id="profileCardTop">
                    	<CardImg variant="top" src="" />
					  	<CardBody className="d-flex flex-column align-items-center text-center">
						    <h4>{data.firstName} {data.lastName}</h4>
						    <p className="text-secondary mb-1">₱{balanceString}</p>
						    <CardLink onClick={() => setModalIsOpen(true)}>Edit Profile</CardLink>
						    {
						    	(data.loginType !== "google")
						    	?
						    	 <p><CardLink onClick={() => setModalIsOpen2(true)}>Edit Password</CardLink></p>
						    	:
						    	null
						    }
					  	</CardBody>
					</Card>
					<Card mt="10" id="profileCard">
						<ListGroup>
							<ListGroup.Item className="d-flex justify-content-between align-items-center flex-wrap">
								<h6 className="mb-0">Email</h6>
                   				<span className="text-secondary">{data.email}</span>
							</ListGroup.Item>
							<ListGroup.Item className="d-flex justify-content-between align-items-center flex-wrap">
								<h6 className="mb-0">Phone number</h6>
                   				<span className="text-secondary">{data.mobileNo}</span>
							</ListGroup.Item>
							<ListGroup.Item className="d-flex justify-content-between align-items-center flex-wrap">
								<h6 className="mb-0">Instagram</h6>
                   				<span className="text-secondary">{data.instagram}</span>
							</ListGroup.Item>
							<ListGroup.Item className="d-flex justify-content-between align-items-center flex-wrap">
								<h6 className="mb-0">Facebook</h6>
                   				<span className="text-secondary">{data.faceBook}</span>
							</ListGroup.Item>
							<ListGroup.Item className="d-flex justify-content-between align-items-center flex-wrap">
								<h6 className="mb-0">Twitter</h6>
                   				<span className="text-secondary">{data.twitter}</span>
							</ListGroup.Item>
							<ListGroup.Item className="d-flex justify-content-between align-items-center flex-wrap">
								<h6 className="mb-0">Address</h6>
                   				<span className="text-secondary">{data.address}</span>
							</ListGroup.Item>
						</ListGroup>
						<CardFooter id="cardfooter">
					  		<Row>
					  			<Col className="text-md-right">
						  			<CardLink onClick={() => setSocialsIsActive(true)}>Edit additional details</CardLink>
								</Col>
							</Row>
					  	</CardFooter>
					</Card>
                </Col>
                <Col md="7" mb="3">
					<Records/>
                </Col>
            </Row>

				<Modal show={modalIsOpen} onHide={() => setModalIsOpen(false)} ariaHideApp={false} id="modal">
			    	
					<Modal.Header>
				    	<Modal.Title className="w-100 text-center d-flex justify-content-center">Update Profile</Modal.Title>
				  	</Modal.Header>

			    	<Form onSubmit={(e) => updateProfile(e)}>

			    	<Modal.Body>
			            <Form.Group controlId="userEmail">
			                <Form.Label>Email address</Form.Label>
			                <Form.Control
			                    type="email"
			                    placeholder="Enter email"
			                    value={email}
			                    onChange={(e) => setEmail(e.target.value)}
			                    required
			                />
			            </Form.Group>

			            <Form.Group controlId="firstName">
			                <Form.Label>First Name</Form.Label>
			                <Form.Control
			                    type="name"
			                    placeholder="Enter first name"
			                    value={firstName}
			                    onChange={(e) => setFirstName(e.target.value)}
			                    required
			                />
			            </Form.Group>

			            <Form.Group controlId="lastName">
			                <Form.Label>Last Name</Form.Label>
			                <Form.Control
			                    type="name"
			                    placeholder="Enter last name"
			                    value={lastName}
			                    onChange={(e) => setLastName(e.target.value)}
			                    required
			                />
			            </Form.Group>

			            <Form.Group controlId="balance">
			                <Form.Label>Balance</Form.Label>
			                <Form.Control
			                    type="number"
			                    placeholder="Enter balance"
			                    value={balance}
			                    onChange={(e) => setBalance(e.target.value)}
			                    required
			                />
			            </Form.Group>

			            <Form.Group controlId="mobileNo">
			                <Form.Label>Mobile Number</Form.Label>
			                <Form.Control
			                    type="number"
			                    placeholder="Enter mobile number"
			                    value={mobileNo}
			                    onChange={(e) => setMobileNo(e.target.value)}
			                    required
			                />
			            </Form.Group>
			            
			        </Modal.Body>
			    		
			        <Modal.Footer>
			            {
							isActive ?
							<Button variant="primary" type="submit" id="submitBtn" className="">
								Update
							</Button>
							:
							<Button variant="danger" type="submit" id="submitBtn" disabled className="">
								Update
							</Button>
						}
							<Button variant="outline-primary" onClick={() => setModalIsOpen(false)} id="submitBtn" className="">
								Cancel
							</Button>
			        </Modal.Footer>
			  
			        </Form>
			    </Modal>

			    <Modal show={modalIsOpen2} onHide={() => setModalIsOpen2(false)} ariaHideApp={false} id="modal">
			    	<Modal.Header closeButton>
				    	<Modal.Title className="w-100 text-center d-flex justify-content-center">Update Password</Modal.Title>
				  	</Modal.Header>

			    	<Form onSubmit={(e) => updatePassword(e)}>
			    	<Modal.Body>
			            <Form.Group controlId="oldPassword">
			                <Form.Label>Old Password</Form.Label>
			                <Form.Control 
			                    type="password" 
			                    placeholder="Password"
			                    value={oldPassword}
			                    onChange={(e) => setOldPassword(e.target.value)}
			                    required
			                />
			            </Form.Group>

			            <Form.Group controlId="newPassword">
			                <Form.Label>New Password</Form.Label>
			                <Form.Control 
			                    type="password" 
			                    placeholder="Password"
			                    value={newPassword}
			                    onChange={(e) => setNewPassword(e.target.value)}
			                    required
			                />
			            </Form.Group>

			            <Form.Group controlId="verifyPassword">
			                <Form.Label>Verify Password</Form.Label>
			                <Form.Control 
			                    type="password" 
			                    placeholder="Password"
			                    value={verifyPassword}
			                    onChange={(e) => setVerifyPassword(e.target.value)}
			                    required
			                />
			            </Form.Group>
		            </Modal.Body>
		            <Modal.Footer>
			            {
			            	isActive2 ?
			                <Button variant="primary" type="submit" id="submitBtn">
			                    Submit
			                </Button>
			                :
			                <Button variant="danger" type="submit" id="submitBtn" disabled>
			                    Submit
			                </Button>
			            }
			            <Button variant="outline-primary" onClick={() => setModalIsOpen2(false)} id="submitBtn" className="">
								Cancel
						</Button>
		            </Modal.Footer>
			        </Form>
			    </Modal>

			    <Modal show={socialsIsActive} onHide={() => setSocialsIsActive(false)} id="modal">
			    	<Modal.Header>
				    	<Modal.Title className="w-100 text-center d-flex justify-content-center">Add Social Media Accounts</Modal.Title>
				  	</Modal.Header>

				  	<Form onSubmit={(e) => addInstagram(e)}>

				  	<Modal.Body>
			            <Form.Group controlId="instagram">
			                <Form.Label>Instagram</Form.Label>
			                <Form.Control 
			                    type="string"
			                    placeholder="Instagram"
			                    value={instagram}
			                    onChange={(e) => setInstagram(e.target.value)}			                    
			                />
			            </Form.Group>
			            <Form.Group controlId="facebook">
			                <Form.Label>Instagram</Form.Label>
			                <Form.Control 
			                    type="string"
			                    placeholder="Facebook"
			                    value={facebook}
			                    onChange={(e) => setFacebook(e.target.value)}			                    
			                />
			            </Form.Group>
			            <Form.Group controlId="twitter">
			                <Form.Label>Instagram</Form.Label>
			                <Form.Control 
			                    type="string"
			                    placeholder="Twitter"
			                    value={twitter}
			                    onChange={(e) => setTwitter(e.target.value)}			                    
			                />
			            </Form.Group>

			            <Form.Group controlId="address">
			                <Form.Label>Address</Form.Label>
			                <Form.Control 
			                    type="string"
			                    placeholder="Address"
			                    value={address}
			                    onChange={(e) => setAddress(e.target.value)}
			                />
			            </Form.Group>

		            </Modal.Body>
		            <Modal.Footer>
			            {
			            	isActive2 ?
			                <Button variant="primary" type="submit" id="submitBtn">
			                    Submit
			                </Button>
			                :
			                <Button variant="danger" type="submit" id="submitBtn">
			                    Submit
			                </Button>
			            }
			            <Button variant="outline-primary" onClick={() => setModalIsOpen2(false)} id="submitBtn" className="">
								Cancel
						</Button>
		            </Modal.Footer>
		            </Form>
			    </Modal>
        	</React.Fragment>
		)
}