import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import Link from 'next/link';
import View from '../../components/View';
import AppHelper from '../../app-helper';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
import {Card, CardText, CardBody, CardTitle, CardSubtitle, CardImg, CardLink, CardFooter} from 'reactstrap';

import { useCookies } from "react-cookie"

export default function index() {
    return(
        <View title={ ' Login ' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <LoginForm/>
                </Col>
            </Row>
        </View>
        )
}


const LoginForm = () => {

    const { user, setUser } = useContext(UserContext);

    const [cookie, setCookie] = useCookies(["token"])

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);
    
    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }
        fetch(`${ AppHelper.API_URL }/users/login`, options)
        .then(AppHelper.toJSON)
        .then(data => {

            if (typeof data.accessToken !== 'undefined') {
                console.log(data.accessToken)                    
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if ( data.error === 'does-not-exist' ) {

                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                
                } else if ( data.error === 'incorrect-password' ) {
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                } else if ( data.error === 'login-type-error' ) {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }

        })

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    const authenticateGoogleToken = (response) => {
        console.log(response);

        const payload = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${ AppHelper.API_URL }/users/verify-google-id-token`, payload )
        .then(AppHelper.toJSON)
        .then(data => {

            if ( typeof data.accessToken !== 'undefined' ) {
                localStorage.setItem("token", data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if ( data.error == 'google-auth-error' ) {
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed.', 'error' )
                } else if ( data.error === 'login-type-error' ) {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure.', 'error')
                }
            }

        })

    };

    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }`}
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            console.log(data)
            setUser({ id: data._id, isAdmin: data.isAdmin, token: accessToken, data: JSON.stringify(data)})
            localStorage.setItem("id", data._id)
            localStorage.setItem("isAdmin", data.isAdmin)
            localStorage.setItem('data', JSON.stringify(data))
            Router.push('/home')
        })

    }

    return (
            <React.Fragment>
            <Card>
                <CardBody id="loginBody">
                <h2 id="title" className="w-100 text-center d-flex justify-content-center">Welcome to Budget Control</h2>
                <h3 id="title" className="w-100 text-center d-flex justify-content-center">Login</h3>
                    <Form onSubmit={(e) => authenticate(e)}>
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>

                        {isActive ? 
                            <Button variant="primary" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">
                                Submit
                            </Button>
                            : 
                            <>
                            <Button variant="danger" type="submit" id="submitBtn" disabled className="w-100 text-center d-flex justify-content-center">
                                Submit
                            </Button>


                            
                            </>
                        }

                        <GoogleLogin
                            //clientId = OAuthClient id from Cloud Google developer platform
                                clientId="321313268725-oj0jgpotjifcbr6d5o9ccbn192tev99h.apps.googleusercontent.com"
                                buttonText="Login"
                                //onSuccess = it runs a function which returns a Google User object which provides access to all of the Google USer methods and details
                                onSuccess={ authenticateGoogleToken }
                                onFailure={ authenticateGoogleToken } // modify on Failed
                                //cookiePolicy= determines cookie policy for the origin of the google login requests
                                cookiePolicy={ 'single_host_origin' }
                                className="w-100 text-center d-flex justify-content-center mt-1"
                                />
                        <Link href="/register">
                            <a className="nav-link w-100 text-center d-flex justify-content-center" role="button" id="registerButton">No account? Register Here</a>
                        </Link>
                    </Form>
                </CardBody>
            </Card>
            </React.Fragment>
    )
}

// global variable
/*
localStorage {
    email: "arvin@mail.com"
}
*/