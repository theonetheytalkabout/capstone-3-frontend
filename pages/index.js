import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import NaviBar from '../components/NaviBar';
import View from '../components/View';

export default function Home(){

  const data = {
    title: "Budget Control",
    content: "Manage your expenses",
    destination: "/register",
    label: "Sign Up Now!"
  }

  return (
    <React.Fragment>
      <View id="body" title={ ' Welcome to Budget Control ' }>
        <Banner data={data}/>
      </View>
    </React.Fragment>
  )
}