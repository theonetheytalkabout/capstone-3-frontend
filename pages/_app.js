import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import NaviBar from '../components/NaviBar';
import Footer from '../components/Footer';
import { UserProvider } from '../UserContext';
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';

function MyApp({ Component, pageProps }) {

	// State hook for the user details
	const [user, setUser] = useState({
		id: null,
		isAdmin: null,
		token: null,
		data: null
	})

	// Function for clearing the localStorage
	const unsetUser = () => {

		localStorage.clear();

		// Changes the value of the user state back to its original value
		setUser({
			id: null,
			isAdmin: null,
			token: null,
			data: null
		});

	}

	useEffect(() => {

		setUser({
			id: localStorage.getItem('id'),
			token: localStorage.getItem('token'),
			data: localStorage.getItem('data'),
			// Added a condition to convert the string data type into boolean
			isAdmin: localStorage.getItem('isAdmin') === 'true'
		})

	}, [])

	return (
  		<React.Fragment>
  			<UserProvider value={{user, setUser, unsetUser}}>
  				<NaviBar />
  				<Container className="">
 					<Component {...pageProps} />
 				</Container>
 				<Footer />
 			</UserProvider>
 		</React.Fragment>
  	)
}

export default MyApp
