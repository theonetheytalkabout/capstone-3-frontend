import React, { useContext, useState, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../components/View';
import { Form, Button, Row, Col, Table } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';
import Link from 'next/link'
import {Card, CardText, CardBody, CardTitle, CardSubtitle, CardImg, CardLink} from 'reactstrap';
import moment from 'moment';

import CategoryData from '../../components/CategoryType';

export default function index() {
	return (
		<View title={ ' Register ' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <Categories/>
                </Col>
            </Row>
        </View>
		)
}

const Categories = () => {

	const { user } = useContext(UserContext);
	const [filter, setFilter] = useState('');

	const [categories, setCategories] = useState([]);

	useEffect(() => {

    	fetch(`${ AppHelper.API_URL }/users/details`, {
        	method: 'GET',
        	headers: { 'Content-Type': 'application/json',
        	'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
	    	})
	    	.then(AppHelper.toJSON)
			.then(data => {
				setCategories(data.categories)
		})
	}, [])

	console.log(categories)

	const categoryData = categories.map(data => {
    	console.log(data)

    	if (filter === "Income" && data.type === filter) {
			return <CategoryData key={data._id} category={data}/>
		} else if (filter === "Expense" && data.type === filter) {
			return <CategoryData key={data._id} category={data}/>
		} else if (filter !== "Income" && filter !== "Expense") {
			return <CategoryData key={data._id} category={data}/>
	  	}

    })

	return (
		<React.Fragment>
            <Head>
                <title>Categories</title>
            </Head>
            
		  	<Card>
		  		<CardBody>
		  			<h3 className="w-100 text-center d-flex justify-content-center">Categories</h3>
			  		<Link href="/categories/create">
					  	<Button variant="dark" type="submit" id="submitBtn" to="/categories/create" className="w-100 text-center d-flex justify-content-center">
								Add Category
						</Button>
			  		</Link>
				  	<Form.Group controlId="type">
						<div>
						<select className="w-100 text-center d-flex justify-content-center"
							value={filter}
							onChange={(e) => setFilter(e.target.value)}
							required
						>
							<option value="">All categories</option>
							<option value="Income">Income</option>
							<option value="Expense">Expense</option>
						</select>
						</div>	
					</Form.Group>
				  	<Table striped bordered hover className="text-center">
						<thead>
							<tr>
								<th>Category Name</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
							{categoryData}
						</tbody>
					</Table>
				</CardBody>
			</Card>

		  
		</React.Fragment>
	)


}