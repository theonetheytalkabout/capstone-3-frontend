import React, { select, useContext, useState, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../../components/View';
import { Form, Button, Row, Col } from 'react-bootstrap';
import AppHelper from '../../../app-helper';
import UserContext from '../../../UserContext';

export default function index() {
	return (
		<View title={ ' Add Category ' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>Add Category</h3>
                    <Categories/>
                </Col>
            </Row>
        </View>
		)
}

const Categories = () => {

const { user } = useContext(UserContext);
console.log(user.token)
	
// State hooks to store the values of the input fields
	const [category, setCategory] = useState('');
	const [type, setType] = useState('Income');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	// console.log(category);
	// console.log(type);

	function addCategory(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', 'Authorization' : `Bearer ${user.token}` },
			body: JSON.stringify({
				category: category,
				type: type
			})
		}

		fetch(`${ AppHelper.API_URL }/users/addCategory`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			Router.push('/categories')
		})

	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match
		if(category !== "" && type !== ""){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [category, type])

	return (
		<React.Fragment>
            <Head>
                <title>Categories</title>
            </Head>
					
		<Form onSubmit={(e) => addCategory(e)}>
			<Form.Group controlId="category">
				<Form.Label>Category Name</Form.Label>
				<Form.Control
					type="category"
					placeholder="Enter category name"
					value={category}
					onChange={(e) => setCategory(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="type">
				<Form.Label>Category Type</Form.Label>
				<div>
				<select className="w-100 text-center d-flex justify-content-center"
					value={type}
					onChange={(e) => setType(e.target.value)}
					required
				>
					<option value="Income">Income</option>
					<option value="Expense">Expense</option>
				</select>
				</div>	
			</Form.Group>

			{isActive ?
				<Button variant="primary" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled className="w-100 text-center d-flex justify-content-center">
					Submit
				</Button>
			}

			
		</Form>
		</React.Fragment>
	)


}