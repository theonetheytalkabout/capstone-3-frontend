import React, { useContext, useState, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../../components/View';
import { Form, Button, Row, Col } from 'react-bootstrap';
import AppHelper from '../../../app-helper';
import UserContext from '../../../UserContext';
import moment from 'moment';
import {Card, CardText, CardBody, CardTitle, CardSubtitle, CardImg, CardLink, CardFooter} from 'reactstrap';
import Modal from 'react-modal';


export default function index() {
	return (
		<View title={ ' Add Record ' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <Categories/>
                </Col>
            </Row>
        </View>
		)
}

const Categories = () => {

	const { user } = useContext(UserContext);
	
	// State hooks to store the values of the input fields
	const [description, setDescription] = useState('');
	const [type, setType] = useState('');
	const [price, setPrice] = useState('');
	const [category, setCategory] = useState('');
	// console.log(category)
	const [expenseCategories, setExpenseCategories] = useState([]);
	const [incomeCategories, setIncomeCategories] = useState([]);


	const [transactionDate, setTransactionDate] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	const [modalIsOpen, setModalIsOpen] = useState(false)
	
	// console.log(description);
	// console.log(type);
	// console.log(transactionDate)

	const [categoryAdd, setCategoryAdd] = useState('');
	// console.log(categoryAdd)
	const [typeAdd, setTypeAdd] = useState('Income');
	// console.log(typeAdd)
	const [isActive2, setIsActive2] = useState(false);

	function addCategory(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', 'Authorization' : `Bearer ${user.token}` },
			body: JSON.stringify({
				category: categoryAdd,
				type: typeAdd
			})
		}

		fetch(`${ AppHelper.API_URL }/users/addCategory`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			// console.log(data)
			setModalIsOpen(false)
		})

	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match
		if(categoryAdd !== "" && typeAdd !== ""){
			setIsActive2(true);
		} else {
			setIsActive2(false);
		}

	}, [categoryAdd, typeAdd])

	function addRecord(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', 'Authorization' : `Bearer ${localStorage.getItem('token')}` },
			body: JSON.stringify({
				description: description,
				type: type,
				price: price,
				category: category,
				transactedOn: transactionDate
			})
		}

		fetch(`${ AppHelper.API_URL }/users/addRecord`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			// console.log(data)
			Router.push('/records')
		})

	}

	useEffect(() => {

    	fetch(`${ AppHelper.API_URL }/users/details`, {
        	method: 'GET',
        	headers: { 'Content-Type': 'application/json',
        	'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
	    	})
	    	.then(AppHelper.toJSON)
			.then(data => {
				let eCategories = []
				let iCategories = []

				data.categories.map(category => {
					if (category.type == "Income") {
						iCategories.push(category)
					} else if (category.type == "Expense") {
						eCategories.push(category)
					}
				})

				setExpenseCategories(eCategories)
				setIncomeCategories(iCategories)
		})
	}, [modalIsOpen])

	useEffect(() => {

		if(description !== "" && (type == "Income" || type == "Expense" )){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [description, type])

	return (
		<React.Fragment>
            <Head>
                <title>Add Record</title>
            </Head>
			<Card>
				<CardBody id="AddRecordBody">
					<h3 id="title" className="w-100 text-center d-flex justify-content-center">Add Record</h3>
					<Form onSubmit={(e) => addRecord(e)}>
					<Form.Group controlId="description">
						<Form.Label>Description</Form.Label>
						<Form.Control
							type="description"
							placeholder="Description"
							value={description}
							onChange={(e) => setDescription(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="type">
						<Form.Label>Category Type</Form.Label>
						<div>
						<select className="w-100 text-center d-flex justify-content-center"
							value={type}
							onChange={(e) => setType(e.target.value)}
							required
						>

							<option value="" disabled>Choose category type</option>
							<option value="Income">Income</option>
							<option value="Expense">Expense</option>
						</select>
						</div>	
					</Form.Group>

					<Form.Group controlId="name">
						<Form.Label>Category Name</Form.Label>
						<div>
						<select className="w-100 text-center d-flex justify-content-center" aria-label=".form-select-lg example"
							value={category}
							onChange={(e) => setCategory(e.target.value)}
							required
						>
							<option value="" disabled>Choose category name</option>

							{
								(type !== "")
								?
									(type == "Income")
									?
									incomeCategories.map(data => (
									<option key={data._id} value={data.name}>{data.name}</option>
								))
									:
									expenseCategories.map(data => (
									<option key={data._id} value={data.name}>{data.name}</option>
								))
								:
								null
							}

						</select>
						</div>
						<CardLink className="float-right" onClick={() => setModalIsOpen(true)}>Add Category</CardLink>
					</Form.Group>

					<Form.Group controlId="price">
						<Form.Label>Price</Form.Label>
						<Form.Control
							type="number"
							placeholder="0"
							value={price}
							onChange={(e) => setPrice(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="transactionDate">
						<Form.Label>Date</Form.Label>
						<Form.Control
							type="date"
							onChange={(e) => setTransactionDate(e.target.value)}
							required
						/>
					</Form.Group>

					{isActive ?
						<Button className="w-100 text-center d-flex justify-content-center" variant="primary" type="submit" id="submitBtn">
							Submit
						</Button>
						:
						<Button className="w-100 text-center d-flex justify-content-center" variant="danger" type="submit" id="submitBtn" disabled>
							Submit
						</Button>
					}

					
				</Form>
			</CardBody>
		</Card>


				<Modal isOpen={modalIsOpen} onRequestClose={() => setModalIsOpen(false)} ariaHideApp={false} id="addModal">
			    	<h4>Add Category</h4>
			    	<Form onSubmit={(e) => addCategory(e)}>
						<Form.Group controlId="category">
							<Form.Label>Category Name</Form.Label>
							<Form.Control
								type="category"
								placeholder="Enter category name"
								value={categoryAdd}
								onChange={(e) => setCategoryAdd(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId="type">
							<Form.Label>Category Type</Form.Label>
							<div>
							<select className="w-100 text-center d-flex justify-content-center"
								value={typeAdd}
								onChange={(e) => setTypeAdd(e.target.value)}
								required
							>
								<option value="Income">Income</option>
								<option value="Expense">Expense</option>
							</select>
							</div>	
						</Form.Group>

						{isActive2 ?
							<Button variant="primary" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center">
								Submit
							</Button>
							:
							<Button variant="danger" type="submit" id="submitBtn" disabled className="w-100 text-center d-flex justify-content-center">
								Submit
							</Button>
						}
					</Form>
			    	<CardLink onClick={() => setModalIsOpen(false)}>Close</CardLink>
			    </Modal>



		</React.Fragment>
	)


}