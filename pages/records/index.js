import React, { useContext, useState, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import View from '../../components/View';
import { Form, Button, Row, Col } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';
import Link from 'next/link'
import ListGroup from 'react-bootstrap/ListGroup';

import {Card, CardText, CardBody, CardTitle, CardSubtitle, CardImg, CardLink, CardFooter} from 'reactstrap';
import RecordData from '../../components/RecordData';

export default function index() {
	return (
		<div>
			<View title={ ' Records ' } >
	            <Row className="justify-content-center">
	                <Col xs md="8">
	                    <Records/>
	                </Col>
	            </Row>
	        </View>
        </div>
		)
}

const Records = () => {

	const [filter, setFilter] = useState('');
	const [search, setSearch] = useState('');
	const [transactions, setTransactions] = useState([]);
	const [balance, setBalance] = useState(0);

	const [term, setTerm] = useState('');

	const [modalIsOpen2, setModalIsOpen2] = useState(true);
	console.log(modalIsOpen2)

    useEffect(() => {

    	fetch(`${ AppHelper.API_URL }/users/details`, {
        	method: 'GET',
        	headers: { 'Content-Type': 'application/json',
        	'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
	    	})
	    	.then(AppHelper.toJSON)
			.then(data => {
				//console.log(data)
				setBalance(data.balance)
				setTransactions(data.transactions.reverse())
				setModalIsOpen2(true)
		})
	}, [modalIsOpen2])

    // console.log(transactions)

    const recordsData = transactions.map(data => {
    	console.log(data.type)

    	if (data.type === filter) {
			if (term !== '') {

				if (data.description.toLowerCase().includes(term.toLowerCase())) {
					return <RecordData key={data._id} record={data} setModalIsOpen2={setModalIsOpen2}/>
				} else {
					return null
				}

			} else {

				return <RecordData key={data._id} record={data} />

			}
		} else if (filter !== "Income" && filter !== "Expense") {

			if (term !== '') {

				if ((data.description.toLowerCase().includes(term.toLowerCase())) || (data.status.toLowerCase().includes(term.toLowerCase())) || (data.category.toLowerCase().includes(term.toLowerCase()))) {
					return <RecordData key={data._id} record={data} setModalIsOpen2={setModalIsOpen2}/>
				} else {
					return null
				}

			} else {

				return <RecordData key={data._id} record={data} setModalIsOpen2={setModalIsOpen2}/>

			}

			
	  	}
    })

	return (
		<React.Fragment>
            <Head>
                <title>Records</title>
            </Head>
            <Card>
            <CardBody id="recordBody">
            	<h3 id="title" className="w-100 text-center d-flex justify-content-center">Records</h3>
            	<h3 className="w-100 text-center d-flex justify-content-center">Current balance: ₱{balance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</h3>
	            	<Row>
	            		<Col>
				            <Link href="/records/add">
						  		<Button variant="dark" type="submit" id="submitBtn" className="w-100 text-center d-flex justify-content-center" to="/categories/create">
									Add Record
								</Button>
						    </Link>
					    </Col>
					    <Col>
						    <Form.Group controlId="search">
			                    <Form.Control 
			                        type="text" 
			                        placeholder="Search Records"
			                        onChange={(e) => setTerm(e.target.value)}
			                        required
			                    />
			          		</Form.Group>
						</Col>
					</Row>
						<Form.Group controlId="type">
							<div>
							<select className="w-100 text-center d-flex justify-content-center"
								value={filter}
								onChange={(e) => setFilter(e.target.value)}
								required
							>
								<option value="">All categories</option>
								<option value="Income">Income</option>
								<option value="Expense">Expense</option>
							</select>
							</div>	
						</Form.Group>

					  	{(transactions < 1) ?
					  		<h3 className="w-100 text-center d-flex justify-content-center">You have no records.</h3>
					  		:
					  		recordsData
					  	}

						
				</CardBody>
            </Card>
		</React.Fragment>
	
	)

}