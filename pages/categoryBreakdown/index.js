import React, { useContext, useState, useEffect } from 'react';
import MonthlyIncome from '../../components/MonthlyIncome';
import MonthlyExpense from '../../components/MonthlyExpense';
import LineChart from '../../components/LineChart';
import IncomePie from '../../components/IncomePie';
import ExpensePie from '../../components/ExpensePie';
import IncomeBoth from '../../components/IncomeBoth'
import View from '../../components/View';
import {Card, CardText, CardBody, CardTitle, CardSubtitle, CardImg, CardLink} from 'reactstrap';
import { Container, Row, Col } from 'react-bootstrap';
import ListGroup from 'react-bootstrap/ListGroup';

export default function index() {

	return (
		<React.Fragment>
			<View title={ ' Statistics ' } className="d-flex justify-content-center align-items-center flex-column vw-100">
			<Container>
				<Row>
					<Col>
						<Card id="ieBreakdown">
						  	<CardBody className="d-flex flex-column align-items-center text-center">
							    <h4>Income and Expense</h4>
							    <IncomeBoth/>
						  	</CardBody>
						</Card>
					</Col>
				</Row>
				<Row>
					<Col>
						<Card id="cardBreakdown">
						  	<CardBody className="d-flex flex-column align-items-center text-center">
							    <h4>Income Pie</h4>
							    <IncomePie/>
						  	</CardBody>
						</Card>
					</Col>
					<Col>
						<Card id="cardBreakdown">
						  	<CardBody className="d-flex flex-column align-items-center text-center">
							    <h4>Expense Pie</h4>
							    <ExpensePie/>
						  	</CardBody>
						</Card>
					</Col>
				</Row>
				<Row>
					<Col>
						<Card id="cardBreakdown">
							<CardBody className="d-flex flex-column align-items-center text-center">
							    <h4>Monthly Income</h4>
							    <MonthlyIncome/>
						  	</CardBody>
						</Card>
					</Col>
					<Col>
						<Card id="cardBreakdown">
							<CardBody className="d-flex flex-column align-items-center text-center">
							    <h4>Monthly Expense</h4>
							    <MonthlyExpense/>
						  	</CardBody>
						</Card>
					</Col>
				</Row>
				<Card id="cardBreakdown">
					<CardBody className="d-flex flex-column align-items-center text-center">
					    <h4>Balance Trend</h4>
					    <LineChart/>
				  	</CardBody>
				</Card>
			</Container>
			</View>
		</React.Fragment>
	)
}
