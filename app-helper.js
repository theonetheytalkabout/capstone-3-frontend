//Helper file shortens written code in various parts of the project
//If you want to create reusable functions/blocks of code that you can use in your project

module.exports = {
	API_URL: process.env.NEXT_PUBLIC_API_URL,
	getAccessToken: () => localStorage.getItem('token'),
	toJSON: (response) => response.json()
}